// Теоретичні питання:
// 1. Екранування це засіб для роботи зі спецсимволами у коді за допомогою \ ;
// 2. Function expression або Function declaration;
// 3. Hoisting - це підняття змінних та функцій в зону видимості коду.
// Проте якщо змінна необ'явлена та неініційована це призводить до помилки.

// Завдання:

function createNewUser() {
  const name = prompt('What is your name?');
  const surname = prompt('What is your surname?');
  const birthday = prompt('Enter your birthday in format: "dd.mm.yyyy"');
  newUser = {
    firstName: name,
    lastName: surname,
    birthday,
    getLogin() {
      return (this.firstName[0] + this.lastName).toLowerCase();
    },
    set setFirstName(newName) {
      Object.defineProperty(newUser, 'firstName', { writable: true });
      this.firstName = newName;
      Object.defineProperty(newUser, 'firstName', { writable: false });
    },
    set setLastName(newName) {
      Object.defineProperty(newUser, 'lastName', { writable: true });
      this.lastName = newName;
      Object.defineProperty(newUser, 'lastName', { writable: false });
    },
    getAge() {
      const formatBirthday = new Date(
        birthday.slice(6),
        birthday.slice(3, 5) - 1,
        birthday.slice(0, 2)
      );
      const now = new Date();
      const age = Math.floor(
        (now - formatBirthday) / (1000 * 60 * 60 * 24 * 365.25)
      );

      return 'Your age is ' + age;
    },
    getPassword() {
      return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + birthday.slice(6);
    }
  };
  return newUser;
}
createNewUser();
Object.defineProperty(newUser, 'firstName', { writable: false });
Object.defineProperty(newUser, 'lastName', { writable: false });
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());
